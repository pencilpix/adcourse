import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { State } from '../../store/state';
import * as fromCourses from '../../store/selectors';
import { instructorOptions, courseOptions } from '../../../utils/create-options';
import {
  LoadCourses,
  UpdateIndicator,
  ResetIndicator,
  Loading
} from '../../store/actions';
import { BehaviorSubject, Subscription, fromEvent } from 'rxjs';
import { debounceTime, throttleTime, map } from 'rxjs/operators';



@Component({
  selector: 'app-courses-home',
  templateUrl: './courses-home.component.html',
  styleUrls: ['./courses-home.component.sass'],
})

export class CoursesHomeComponent implements OnInit, OnDestroy {
  @ViewChild('formCard') $formCard: ElementRef;
  courses$ = this.store.pipe(select(fromCourses.courses));
  hasMore$ = this.store.pipe(select(fromCourses.hasMore));
  end$ = this.store.pipe(select(fromCourses.end));
  loading$ = this.store.pipe(select(fromCourses.loading));
  page$ = this.store.pipe(select(fromCourses.page));
  lastStyle: {[k: string]: any } = { top: '6rem' };
  lastScroll = 0;
  style$ = new BehaviorSubject(this.lastStyle);
  instructorOptions = instructorOptions;
  courseOptions = courseOptions;
  scrollSub: Subscription;


  constructor(private store: Store<State>) {}

  ngOnInit() {
    this.scrollSub = fromEvent(window, 'scroll')
      // .pipe(debounceTime(500))
      // .pipe(throttleTime(50))
      .subscribe(() => this.onScroll());
    this.onScroll();
    this.setCardWidth();
  }

  ngOnDestroy(): void {
    if (this.scrollSub && this.scrollSub.unsubscribe) {
      this.scrollSub.unsubscribe();
    }
  }

  setCardWidth() {
    const $el = this.$formCard.nativeElement;
    const $parent = $el && $el.parentNode;
    this.lastStyle.width = $parent.offsetWidth + 'px';
    this.style$.next(this.lastStyle);
  }

  onScroll() {
    const $el = this.$formCard.nativeElement;
    const { scrollY, innerHeight } = window;
    const  { bottom, height } = $el && $el.getBoundingClientRect();
    const bottomVisible = bottom < (innerHeight - 120) && bottom > innerHeight / 3;
    const isLarge = height > innerHeight - 120;
    const maxPos = Math.min(height - innerHeight + 120, scrollY);
    let newPos = null;

    if (!isLarge) {
      newPos = '96px';
    } else {
      newPos = bottomVisible ? this.lastStyle.top : (96 - maxPos) + 'px';
    }

    if (this.lastStyle.top !== newPos) {
      this.lastStyle.top = newPos;
      this.style$.next(this.lastStyle);
    }
  }

  onLoadMore(currentPage) {
    const page = currentPage + 1;
    if (!page) { return; }
    this.store.dispatch(new LoadCourses(page));
  }

  onStartLoading() {
    this.store.dispatch(new Loading(true));
  }

  onUpdateIndicator() {
    this.store.dispatch(new UpdateIndicator());
  }

  onResetIndicator() {
    this.store.dispatch(new ResetIndicator);
  }
}
