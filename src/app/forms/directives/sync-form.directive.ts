import { Directive, Input, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { FormsInputControlService } from '../forms-input-control.service';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';



@Directive({
  selector: '[appSyncForm]'
})
export class SyncFormDirective implements OnInit, OnDestroy {
  // tslint:disable-next-line:no-input-rename
  @Output() syncValues = new EventEmitter();
  @Input() activeStep: number;
  @Input() syncDelay = 500;
  @Input('appSyncForm')
  set Data(inputs: any) {
    if (!inputs) { return; }

    if (this.lastStep !== this.activeStep) {
      this.removeAllControls();
      this.lastStep = this.activeStep;
    }

    inputs.forEach(input => {
      const { form } = this.formGroup;
      const inputInstance = this.inputControl.toFormControl(input);
      if (!form.get(input.key)) {
        form.addControl(input.key, inputInstance);
      }

      if (form.get(input.key).value !== input.value) {
        form.controls[input.key].reset();
        form.patchValue({ [input.key]: input.value });
      }
      this.isStoreUpdates = true;
    });
  }

  lastStep = null;
  formGroupSub: Subscription;
  isStoreUpdates = false;


  constructor(
    private formGroup: FormGroupDirective,
    private inputControl: FormsInputControlService,
  ) { }


  ngOnInit() {
    this.formGroupSub = this.formGroup.form
      .valueChanges
      .pipe(debounceTime(this.syncDelay))
      .subscribe((values) => this.onChange(values));
  }

  ngOnDestroy() {
    if (this.formGroupSub && this.formGroupSub.unsubscribe) {
      this.formGroupSub.unsubscribe();
    }
  }


  onChange(values) {
    if (!this.isStoreUpdates) {
      this.syncValues.emit(values);
    } else {
      this.isStoreUpdates = false;
    }
  }


  removeAllControls() {
    const keys = Object.keys(this.formGroup.form.controls);
    keys.forEach(key => this.formGroup.form.removeControl(key));
    this.formGroup.form.reset();
  }
}
