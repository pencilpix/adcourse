import { Action } from '@ngrx/store';
import { ActivatedRouteSnapshot } from '@angular/router';

export const SET_TITLE = '[layout] set title';
export const SET_URL = '[layout] set url';
export const SET_URL_SUCCESS = '[layout] set url success';


export class SetTitle implements Action {
  readonly type = SET_TITLE;

  constructor(public payload: string) {}
}

export class SetUrl implements Action {
  readonly type = SET_URL;

  constructor(public payload: ActivatedRouteSnapshot) {}
}

export class SetUrlSuccess implements Action {
  readonly type = SET_URL_SUCCESS;

  constructor(public payload: string) {}
}


export type Actions = SetTitle |
                      SetUrl |
                      SetUrlSuccess;
