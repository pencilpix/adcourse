import { Action } from '@ngrx/store';

/**
 * NOTE:
 * ngrx is not fully sync with the reactive form yet
 * @TODO: (@hassan) please improve it when needed.
 * 1. needs validation to be also sync
 * 2. needs status to be sync touch, pristine ... etc
 */

export const FORMS_ADD_STEPS = '[forms] Add Steps';
export const FORMS_LOADING = '[forms] Set loading';
export const FORMS_GO_TO = '[forms] Go To Step';
export const FORMS_SAVE = '[forms] Save Form';
export const FORMS_SAVE_SUCCESS = '[forms] Save Form Success';
export const FORMS_SAVE_FAILURE = '[forms] Save Form Failure';
export const FORMS_RESET = '[forms] Reset Form';
export const FORMS_UPDATE_VALUES = '[forms] Update form Values';



export class AddSteps implements Action {
  readonly type = FORMS_ADD_STEPS;
  constructor(public payload: any) {}
}


export class GoToStep implements Action {
  readonly type = FORMS_GO_TO;
  constructor(public payload: number) {}
}


export class Loading implements Action {
  readonly type = FORMS_LOADING;
  constructor(public payload: boolean) {}
}


export class Save implements Action {
  readonly type = FORMS_SAVE;
  constructor() {}
}


export class SaveSuccess implements Action {
  readonly type = FORMS_SAVE_SUCCESS;
  constructor() {}
}


export class SaveFailure implements Action {
  readonly type = FORMS_SAVE_FAILURE;
  constructor(public payload: any) {}
}


export class Reset implements Action {
  readonly type = FORMS_RESET;
  constructor() {}
}


export class UpdateValues implements Action {
  readonly type = FORMS_UPDATE_VALUES;
  constructor(public payload: { [k: string]: any }) {}
}


export type Actions =
  AddSteps |
  GoToStep |
  Loading |
  Save |
  SaveSuccess |
  SaveFailure |
  Reset |
  UpdateValues;
