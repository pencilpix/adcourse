export interface Course {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  title: string;
  description: string;
  avatar: string;
  image: string;
  startAt: string;
  endAt: string;
  minSeats: number;
  maxSeats: number;
}


export interface CourseData {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  title: string;
  description: string;
  avatar: string;
  image: string;
  start_at: string;
  end_at: string;
  min_seats: number;
  max_seats: number;
}

export interface CoursePagination {
  total: number;
  page: number;
  first: number;
  last: number;
}

export interface CoursesResponse extends CoursePagination {
  list: Course[];
}

export interface CoursesDataResponse extends CoursePagination {
  list: CourseData[];
}

