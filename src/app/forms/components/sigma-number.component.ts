import { Component } from '@angular/core';
import { InputComponent } from './input.component';

@Component({
  selector: 'app-sigma-number',
  template: `
    <div [formGroup]="form" class="adcourse-form__input">
      <mat-form-field>
        <input
            [placeholder]="formInput.label"
            [formControlName]="formInput.key"
            matInput
            [type]="formInput.type">

        <mat-error
            *ngIf="controls.errors?.required">
            {{formInput.label}} is <strong>required</strong>
        </mat-error>

        <mat-error *ngIf="controls.errors?.min">
          {{formInput.label}}'s value must be greater than or equal {{controls.errors.min?.min}}
        </mat-error>

        <mat-error *ngIf="controls.errors?.max">
          {{formInput.label}}'s value must be less than or equal {{controls.errors.max?.max}}
        </mat-error>
      </mat-form-field>
    </div>
  `,
  styleUrls: ['./input.component.sass'],
})
export class SigmaNumberComponent extends InputComponent {

  constructor() { super(); }
}
