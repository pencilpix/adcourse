import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { Action, select, Store } from '@ngrx/store';
import { switchMap, withLatestFrom, catchError } from 'rxjs/operators';
import {
  SaveSuccess,
  SaveFailure,
  Loading,
  Save,
  FORMS_SAVE,
} from '../../forms/store/actions';
import { values, State } from './index';
import { CreateCourseService } from '../create-course.service';


@Injectable()
export class CreateCourseEffects {
  @Effect()
  save$: Observable<Action> = this.actions$.pipe(
    ofType<Save>(FORMS_SAVE),
    withLatestFrom(this.store.pipe(select(values))),
    switchMap(([action, data]) => this.service.addCourse(data)),
    switchMap((res) => {
      this.service.showMessage('Your ad is published successfully');
      return [
        new SaveSuccess(),
        new Loading(false),
      ];
    }),

    catchError((err) => {
      const msg = 'something went wrong please try again later';
      this.service.showMessage(msg);
      return of(new SaveFailure(msg));
    })
  );

  constructor(
    private actions$: Actions,
    private store: Store<State>,
    private service: CreateCourseService,
  ) {}
}
