import { createFeatureSelector, createSelector } from '@ngrx/store';
import { FormsState } from './state';
import { InputTypeOptions } from '../models';

export function createFormsSelectors<T>(featureName) {
  const featureState = createFeatureSelector<FormsState<T>>(featureName);
  const steps = createSelector(
    featureState,
    (state: FormsState<T>) => state.steps
  );

  const stepIndex = createSelector(
    featureState,
    (state: FormsState<T>) => state.selectedStep + 1
  );

  const stepsNo = createSelector(
    featureState,
    (state: FormsState<T>) => state.stepsNo
  );

  const step =  createSelector(
    featureState,
    (state: FormsState<T>): any => state.steps[state.selectedStep]
  );

  const loading = createSelector(
    featureState,
    (state: FormsState<T>) => state.loading
  );

  const errors = createSelector(
    featureState,
    (state: FormsState<T>) => state.errors
  );

  const error = createSelector(
    featureState,
    (state: FormsState<T>, props: any) => state.errors[props.name]
  );

  const values = createSelector(
    featureState,
    (state: FormsState<T>) => Object.values(state.values)
      .reduce((c, n) => ({...c, ...n}), {})
  );

  const value = createSelector(
    featureState,
    (state: FormsState<T>, props: any) => state.values[props.name]
  );

  const activeStep = createSelector(
      step,
      values,
      (stepVal: any[], valuesVal) => stepVal.map((item: InputTypeOptions<any>) => {
        const currentVal = values[item.key];
        if (typeof currentVal !== 'undefined') {
          return { ...item, value: currentVal };
        }
        return item;
      })
    );

  const activeStepValue = createSelector(
    activeStep,
    values,
    (list, obj) => {
      const stepValue = {};
      list.forEach(item => stepValue[item.key] = obj[item.key]);
      return stepValue;
    }
  );


  return {
    form: featureState,
    steps,
    step,
    loading,
    errors,
    error,
    values,
    value,
    stepIndex,
    activeStep,
    activeStepValue,
    stepsNo
  };
}

