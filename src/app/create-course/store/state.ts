import { FormsState } from '../../forms/store';

export interface State extends FormsState<any> {
}


export const initialState: State = {
  steps: {},
  selectedStep: null,
  values: {},
  errors: {},
  loading: false,
  stepsNo: 0,
};
