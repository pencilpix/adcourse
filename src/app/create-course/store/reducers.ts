import { createReducersHelper } from '../../forms/store';
import { State, initialState } from './state';
import {
  Actions,
  FORMS_ADD_STEPS,
  FORMS_GO_TO,
  FORMS_RESET,
  FORMS_LOADING,
  FORMS_SAVE_SUCCESS,
  FORMS_SAVE_FAILURE,
  FORMS_UPDATE_VALUES,
} from '../../forms/store/actions';


const helpers = createReducersHelper<any>();

export function reducer(state: State = initialState, action: Actions) {
  switch (action.type) {
    case FORMS_ADD_STEPS:
      return helpers.addSteps(action.payload, state);

    case FORMS_GO_TO:
      return helpers.gotoStep(action.payload, state);

    case FORMS_RESET:
      return helpers.resetFormValues(state);

    case FORMS_SAVE_SUCCESS:
      return helpers.resetFormValues(state);

    case FORMS_SAVE_FAILURE:
      return helpers.setError(action.payload, state);

    case FORMS_LOADING:
      return helpers.setLoading(action.payload, state);

    case FORMS_UPDATE_VALUES:
      return helpers.updateValues(action.payload, state);


    default:
      return state;
  }
}
