import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { State } from '../store/state';
import { Router, ActivationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { SetUrl } from '../store/actions';
import { getCurrentUrl, getCurrentTitle } from '../store/selectors';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.sass']
})
export class LayoutComponent implements OnInit, OnDestroy {
  route$: Observable<any>;
  title: Subscription;
  route: Subscription;

  constructor(
    private titleService: Title,
    private store: Store<State>,
    private router: Router,
  ) {
  }


  ngOnInit() {
    this.route = this.router.events
      .pipe(filter((event: any) => event instanceof ActivationEnd))
      .subscribe((route) => this.handleRouteChanges(route.snapshot));

    this.route$ = this.store.pipe(select(getCurrentUrl));
    this.title = this.store.pipe(select(getCurrentTitle))
      .subscribe(title => this.titleService.setTitle(title));
  }

  ngOnDestroy() {
    this.title.unsubscribe();
    this.route.unsubscribe();
  }


  handleRouteChanges(route) {
    const action = new SetUrl(route);
    this.store.dispatch(action);
  }
}
