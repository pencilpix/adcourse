import { routerReducer } from '@ngrx/router-store';
import { ActionReducerMap, MetaReducer, ActionReducer } from '@ngrx/store';
import { State } from './state';
import { environment } from '../../environments/environment';

export const reducers: ActionReducerMap<State> = {
  router: routerReducer,
};


export function debug(reducer: ActionReducer<any>): ActionReducer<any> {
  return function(state, action) {
    console.groupCollapsed('[debug] action: ', action.type);
    console.warn('[state]', state);
    console.warn('[action]', action);
    console.groupEnd();

    return reducer(state, action);
  };
}

export const metaReducers: MetaReducer<any>[] = environment.production ? [] : [debug];
