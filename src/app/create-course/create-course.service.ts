import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { createCourseData } from '../utils/helpers';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class CreateCourseService {
  url = '/api/courses';

  constructor(private http: HttpClient, private snackbar: MatSnackBar) { }

  addCourse(data) {
    const id = Math.floor((Math.random() * 1000) + Date.now());
    const course = createCourseData({ ...data, id });
    return this.http.post(this.url, course);
  }

  showMessage(msg: string, duration?: number, action?: string) {
    this.snackbar.open(msg, action || null, {
      duration: duration || 5000,
      verticalPosition: 'top',
    });
  }
}
