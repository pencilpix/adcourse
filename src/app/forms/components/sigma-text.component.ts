import { Component } from '@angular/core';
import { InputComponent } from './input.component';

@Component({
  selector: 'app-sigma-text',
  template: `
    <div [formGroup]="form" class="adcourse-form__input">
      <mat-form-field
          *ngIf="formInput.type !== 'select' && formInput.type !== 'radio' && formInput.type !== 'checkbox'">
        <input
            *ngIf="formInput.type !== 'textarea'"
            [placeholder]="formInput.label"
            [formControlName]="formInput.key"
            matInput
            type="text">

        <textarea
            *ngIf="formInput.type === 'textarea'"
            [placeholder]="formInput.label"
            [formControlName]="formInput.key"
            matInput
            type="text">
        </textarea>


        <mat-error
            *ngIf="controls.errors?.required">
            {{ formInput.label }} is <strong>required</strong>
        </mat-error>


        <mat-error *ngIf="controls.errors?.pattern && formInput.patternMessage && !controls.errors?.required">
          {{ formInput.label + ' ' + formInput.patternMessage }}
        </mat-error>

        <mat-error *ngIf="controls.errors?.pattern && !formInput.patternMessage && !controls.errors?.required">
          {{formInput.label + ' format is not correct'}}
        </mat-error>

        <mat-error *ngIf="controls.errors?.minlength">
          {{formInput.label}} 's characters no. must be greater than or equal {{controls.errors.minlength?.requiredLength}}
        </mat-error>

        <mat-error *ngIf="controls.errors?.maxlength">
          {{formInput.label}} 's characters no. must be less than or equal {{controls.errors.maxlength?.requiredLength}}
        </mat-error>

      </mat-form-field>
    </div>
  `,
  styleUrls: ['./input.component.sass'],
})
export class SigmaTextComponent extends InputComponent {

  constructor() { super(); }
}
