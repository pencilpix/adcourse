import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Course, CoursePagination } from '../models/course';


export const adapter: EntityAdapter<Course> = createEntityAdapter<Course>();


export interface State extends CoursePagination, EntityState<Course> {
  indicator: number;
  loading: boolean;
  error: string;
}


export const initialState: State = adapter.getInitialState({
  total: 0,
  last: null,
  first: null,
  page: 0,
  indicator: 0,
  loading: false,
  error: null,
});
