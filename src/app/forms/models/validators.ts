import { ValidatorFn, AbstractControl } from '@angular/forms';

function getDate(date): any {
  let newDate;
  if (typeof date === 'object' && date instanceof Date) {
    return date;
  } else if (typeof date === 'string') {
    newDate = new Date(date);
  }

  if (!newDate.getTime()) {
    throw new Error(
      'validators fromDate, toDate must ' +
      'by a valid string date format or an instance of Date'
    );
  }

  return newDate;
}

export class ExternalValidators {
  static fromDate(date: any): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any} | null => {
      const { value } = control;
      const valueDate = new Date(value);
      const valueFromDate = getDate(date);
      const isGreater = valueFromDate.getTime() <= valueDate.getTime();

      return !isGreater ? { 'fromDate': { fromDate: valueFromDate } } : null;
    };
  }

  static toDate(date: any): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any} | null => {
      const { value } = control;
      const valueDate = new Date(value);
      const valueToDate = getDate(date);
      const isLess = valueToDate.getTime() >= valueDate.getTime();

      return !isLess ? { 'toDate': { toDate: valueToDate } } : null;
    };
  }

}
