import { InputTypeOptions } from '../models';

export interface FormSteps<T> {
  [k: number]: InputTypeOptions<T[]>;
}

export interface FormsState <T> {
  steps: FormSteps<T>;
  selectedStep: number;
  values: { [i: string]: any };
  errors: { [i: string]: any };
  loading: false;
  stepsNo: number;
}

