import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from '@angular/core';

import { Course } from '../models/course';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-courses-list',
  template: `
    <div class="adcourse-list__wrap" #listEl>
      <app-courses-list-item
          class="adcourse-list__item-wrap"
          *ngFor="let course of courses"
          [course]="course">
      </app-courses-list-item>

      <div
          class="adcourse-list__loading"
          *ngIf="loading">
        <mat-spinner
            color="accent"
            diameter="40">
        </mat-spinner>
      </div>

      <div class="adcourse-list__end adcourse-text--grayscale-A400" *ngIf="end && !hasMore">
        <p>end of results</p>
      </div>
    </div>
  `,

  styles: [`
    .adcourse-list__loading,
    .adcourse-list__end {
      display: flex;
      flex-direction: column;
      align-items: center;
    }
    .adcourse-list__loading p,
    .adcourse-list__end .mat-spinner {
      margin: -.5rem 0 0
    }
  `]
})
export class CoursesListComponent implements OnInit, OnDestroy {
  @Input() courses: Course[];
  @Input() end: Boolean;
  @Input() loading: Boolean;
  @Input() hasMore: Boolean;
  @Input() page: number;
  @Output() loadMore: EventEmitter<any> = new EventEmitter();
  @Output() updateIndicator: EventEmitter<any> = new EventEmitter();
  @Output() resetIndicator: EventEmitter<any> = new EventEmitter();
  @Output() startLoading: EventEmitter<any> = new EventEmitter();
  @ViewChild('listEl') $listEl: ElementRef;
  scrollSub: Subscription;

  constructor() { }

  ngOnInit() {
    const courses = this.courses || [];
    if (!courses.length && !this.end) {
      this.loadMore.emit();
    }

    this.scrollSub = fromEvent(window, 'scroll')
      .pipe(debounceTime(250))
      .subscribe(() => this.onScroll());

    // work around to avoid
    // error of Expression has changed after it was checked.
    // @TODO: (mohamed) check why this is happinging
    // and fix the error without using set timeout which it is
    // a hacky work around.
    setTimeout(() => this.onScroll(), 500);
  }

  ngOnDestroy() {
    if (this.scrollSub && this.scrollSub.unsubscribe) {
      this.scrollSub.unsubscribe();
    }
    this.resetIndicator.emit();
  }

  onScroll() {
    const $el = this.$listEl.nativeElement;
    const box = $el.getBoundingClientRect();
    const { bottom } = box;
    const hitBottom = bottom <= window.innerHeight;

    if (this.end && !this.hasMore) {
      this.scrollSub.unsubscribe();
      return;
    }

    if (!this.loading && hitBottom && this.hasMore) {
      this.updateIndicator.emit();
    } else if (!this.loading && hitBottom) {
      this.startLoading.emit();
      this.loadMore.emit(this.page);
    }
  }
}
