import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { CoursesService } from '../courses.service';
import { createCourse } from '../../utils/helpers';
import {
  LOAD_COURSES,
  LoadCourses,
  LoadCoursesSuccess,
  LoadCoursesError,
  UpdateIndicator
} from './actions';
import { map, switchMap, catchError } from 'rxjs/operators';


export interface ActionWithPayload extends Action {
  type: string;
  payload: any;
}

@Injectable()
export class CoursesEffects {
  @Effect()
  loadCourses$: Observable<Action> = this.actions$.pipe(
    ofType<LoadCourses>(LOAD_COURSES),
    map(action => action.payload),
    switchMap((page) => this.service.fetchCourses(page)),
    switchMap((res) => [
      new LoadCoursesSuccess({...res, list: res.list.map(createCourse)}),
      new UpdateIndicator()
    ]),
    catchError((err) => of(new LoadCoursesError('could not retrieve courses')))
  );


  constructor(
    private actions$: Actions,
    private service: CoursesService,
  ) {}
}
