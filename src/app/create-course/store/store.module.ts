import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { reducer } from './reducers';
import { CreateCourseEffects } from './effects';


@NgModule({
  imports: [
    CommonModule,
    EffectsModule.forFeature([CreateCourseEffects]),
    StoreModule.forFeature('createCourse', reducer),
  ],
  declarations: []
})
export class CreateCourseStoreModule { }
