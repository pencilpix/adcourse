export interface State {
  title: string;
  // while @ngrx/router-store has an issue
  // trying to simplify alternative solution
  // to check active url and then set page title
  activeUrl: string;
}

export const initialState: State = {
  title: 'ADCOURSE | publish courses',
  activeUrl: '/',
};
