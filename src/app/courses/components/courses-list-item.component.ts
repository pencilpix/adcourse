import { Component, OnInit, Input } from '@angular/core';
import { Course } from '../models/course';

@Component({
  selector: 'app-courses-list-item',
  styleUrls: ['./courses-list-item.component.sass'],
  template: `
    <div class="adcourse-list__item">
      <mat-card>
        <mat-card-header>
          <img mat-card-avatar [src]="course?.avatar">
          <mat-card-title>{{ course?.firstName + ' ' + course?.lastName }}</mat-card-title>
          <mat-card-subtitle>{{course?.firstName + ' ' + course?.lastName}}</mat-card-subtitle>
        </mat-card-header>
        <figure class="adcourse-list__item-img">
          <img mat-card-image [src]="course?.image" [alt]="course?.title">
        </figure>
        <mat-card-content>
          <mat-card-title>{{ course?.title }}</mat-card-title>
          <mat-card-subtitle>Reservation Contact: {{course?.email}}</mat-card-subtitle>

          <p class="adcourse-list__item-desc">{{ course?.description | slice:0:300 }}</p>

          <p class="adcourse-list__item-stats adcourse-text--grayscale-200">
            <mat-icon>event</mat-icon>
            <b>{{ course?.startAt | date }}</b> to
            <b>{{ course?.endAt | date }}</b>
          </p>
          <p class="adcourse-list__item-stats adcourse-text--grayscale-200">
            <mat-icon>event_seat</mat-icon>
            <b>min seats {{ course?.minSeats }}</b> -
            <b>max seats {{ course?.maxSeats }}</b>
          </p>
        </mat-card-content>
      </mat-card>
    </div>
  `,
})
export class CoursesListItemComponent {
  @Input() course: Course;

  constructor() { }
}
