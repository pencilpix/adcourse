import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { layoutReducer } from './reducers';
import { LayoutEffects } from './effects';


@NgModule({
  imports: [
    CommonModule,
    EffectsModule.forFeature([LayoutEffects]),
    StoreModule.forFeature('layout', layoutReducer),
  ],
})
export class LayoutStoreModule { }
