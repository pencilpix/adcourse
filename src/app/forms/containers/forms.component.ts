import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { InputTypeBase } from '../models/input-type';
import { FormsInputControlService } from '../forms-input-control.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.sass']
})
export class FormsComponent {
  @Input() form$: Observable<any>;
  @Input() loading$: Observable<any>;
  @Input() steps: number;
  @Input() currentStep: number;
  @Output() reset = new EventEmitter();
  @Output() prevStep = new EventEmitter();
  @Output() nextStep = new EventEmitter();
  @Output() save = new EventEmitter();
  @Output() newValues = new EventEmitter();

  inputsSub: Subscription;
  formInputs: InputTypeBase<any>[];
  form: FormGroup = this.inputControl.toFormGroup([]);


  constructor(
    private inputControl: FormsInputControlService,
  ) {}


  onNext() {
    this.nextStep.emit();
  }


  onPrev() {
    this.prevStep.emit();
  }


  onReset() {
    this.reset.emit();
  }


  onSubmit(event) {
    if (this.currentStep !== this.steps) {
      event.preventDefault();
      this.onNext();
      return;
    }
    this.save.emit();
  }


  onSyncValues(values) {
    this.newValues.emit(values);
  }


  trackByFn(index, item) {
    return item.key || index;
  }
}
