import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateCourseComponent } from './containers/create-course.component';
import { CreateCourseStoreModule } from './store/store.module';
import { FormModule } from '../forms/form.module';
import { CreateCourseService } from './create-course.service';
import {MatSnackBarModule} from '@angular/material';


@NgModule({
  imports: [
    CommonModule,
    CreateCourseStoreModule,
    FormModule,
    MatSnackBarModule,
  ],
  exports: [CreateCourseComponent],
  providers: [CreateCourseService],
  declarations: [CreateCourseComponent],
})
export class CreateCourseModule { }
