import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesRoutingModule } from './courses-routing.module';
import { CoursesListComponent } from './components/courses-list.component';
import { CoursesListItemComponent } from './components/courses-list-item.component';
import { CoursesComponent } from './containers/courses.component';
import { CoursesHomeComponent } from './containers/courses-home/courses-home.component';
import { CoursesStoreModule } from './store/courses-store.module';
import { CoursesService } from './courses.service';
import { FormModule } from '../forms/form.module';
import { CreateCourseModule } from '../create-course/create-course.module';
import {
  MatButtonModule,
  MatCardModule,
  MatGridListModule,
  MatProgressSpinnerModule,
  MatIconModule
} from '@angular/material';



@NgModule({
  imports: [
    CommonModule,
    CoursesRoutingModule,
    CoursesStoreModule,
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatIconModule,
    CreateCourseModule,
    FormModule,
  ],
  providers: [
    CoursesService,
  ],
  declarations: [
    CoursesListComponent,
    CoursesListItemComponent,
    CoursesComponent,
    CoursesHomeComponent,
  ]
})
export class CoursesModule { }

