import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import {
  SET_URL,
  SetUrl,
  SetTitle,
  SetUrlSuccess
} from './actions';
import { map, switchMap } from 'rxjs/operators';


export interface ActionWithPayload extends Action {
  type: string;
  payload: any;
}

@Injectable()
export class LayoutEffects {
  @Effect()
  setUrl$: Observable<ActionWithPayload> = this.actions$.pipe(
    ofType<SetUrl>(SET_URL),
    map((action) => {
      return action;
    }),
    map((action) => action.payload),
    switchMap(({ url, data }) => [
      ...(data && data.title ? [new SetTitle(data.title)] : []),
      new SetUrlSuccess(url.join('')),
    ])
  );


  constructor(private actions$: Actions) {}
}
