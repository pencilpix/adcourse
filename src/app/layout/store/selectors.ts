import { createSelector, createFeatureSelector } from '@ngrx/store';
import { State } from './state';


export const getLayoutState = createFeatureSelector<State>('layout');

export const getCurrentUrl = createSelector(
  getLayoutState,
  (state: State) => state.activeUrl,
);

export const getCurrentTitle = createSelector(
  getLayoutState,
  (state: State) => state.title,
);
