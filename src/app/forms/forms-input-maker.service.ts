import { Injectable } from '@angular/core';
import * as models from './models';

@Injectable()
export class FormsInputMakerService {
  getInput(inputs: models.InputTypeOptions<any>[]) {
    return inputs.map(input => {
      switch (input.controlType) {
        case 'sigmaEmail':
          return new models.InputSigmaEmail(input);

        case 'sigmaUrl':
          return new models.InputSigmaUrl(input);

        case 'sigmaNumber':
          return new models.InputSigmaNumber(input);

        case 'sigmaDate':
          return new models.InputSigmaDate(input);

        default:
          return new models.InputTypeBase(input);
      }
    }).sort((a, b) => a.order - b.order);
  }
}
