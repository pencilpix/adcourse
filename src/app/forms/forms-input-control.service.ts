import { Injectable } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import {
  InputTypeBase,
  ExternalValidators,
} from './models';

@Injectable()
export class FormsInputControlService {

  builtInValidators = [
    'min',
    'max',
    'required',
    'requiredTrue',
    'email',
    'minLength',
    'maxLength',
    'pattern',
  ];

  externalValidators = [
    'fromDate',
    'toDate',
  ];


  toFormGroup(inputs: InputTypeBase<any>[]) {
    const group: any = {};

    inputs.forEach(input => {
      group[input.key] = this.toFormControl(input);
    });

    return new FormGroup(group);
  }


  toFormControl(input: InputTypeBase<any>) {
    const validators = this.getValidators(input);
    return new FormControl(input.value || '', validators);
  }


  private getValidators(input) {
    const builtIn = this.builtInValidators
      .filter(validator => input[validator])
      .map(validator => this.getValidator(input, validator, Validators));

    const external =  this.externalValidators
      .filter(validator => input[validator])
      .map(validator => this.getValidator(input, validator, ExternalValidators));

    return [...builtIn, ...external];
  }


  private getValidator(input, validator, validators) {
    switch (validator) {
      case 'email':
        return validators[validator];

      case 'required':
        return validators[validator];

      case 'requiredTrue':
        return validators[validator];


      default:
        return validators[validator](input[validator]);
    }
  }
}
