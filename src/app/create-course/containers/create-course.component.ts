import { Component, OnInit, Input } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromCreate from '../store';
import * as formActions from '../../forms/store/actions';
import { FormsInputMakerService } from '../../forms/forms-input-maker.service';


@Component({
  selector: 'app-create-course',
  template: `
      <app-forms
        [loading$]="loading$"
        [form$]="form$"
        [steps]="steps$ | async"
        [currentStep]="(stepIndex$ | async)"
        (newValues)="onNewValues($event)"
        (nextStep)="onGo(1)"
        (prevStep)="onGo(-1)"
        (reset)="onReset()"
        (save)="onSave()">
      </app-forms>
  `,
  styles: []
})

export class CreateCourseComponent implements OnInit {
  @Input() instructorInfo;
  @Input() courseInfo;
  steps$ = this.store.pipe(select(fromCreate.stepsNo));
  showForm = false;
  values$ = this.store.pipe(select(fromCreate.values));
  value$ = this.store.pipe(select(fromCreate.activeStepValue));
  form$ = this.store.pipe(select(fromCreate.activeStep));
  stepIndex$ = this.store.pipe(select(fromCreate.stepIndex));
  loading$ = this.store.pipe(select(fromCreate.loading));


  constructor(
    private store: Store<fromCreate.State>,
    private inputMaker: FormsInputMakerService,
    ) {}


  ngOnInit() {
    this.store.dispatch(
      new formActions.AddSteps({
        0: this.inputMaker.getInput(this.instructorInfo),
        1: this.inputMaker.getInput(this.courseInfo),
      })
    );

    this.showForm = true;
  }


  onSave() {
    const action = new formActions.Save();
    const loading = new formActions.Loading(true);
    this.store.dispatch(loading);
    this.store.dispatch(action);
  }


  onReset() {
    const action = new formActions.Reset();
    this.store.dispatch(action);
  }


  onGo(no) {
    const action = new formActions.GoToStep(no);
    this.store.dispatch(action);
  }


  onNewValues(values) {
    const action = new formActions.UpdateValues(values);
    this.store.dispatch(action);
  }
}
