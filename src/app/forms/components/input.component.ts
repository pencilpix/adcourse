import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';
import { InputTypeBase } from '../models';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.sass']
})
export class InputComponent implements OnInit {
  @Input() formInput: InputTypeBase<any>;
  @Input() form: FormGroup;
  controls: AbstractControl;


  ngOnInit() {
    this.controls = this.form.controls[this.formInput.key];
  }


  get isValid() {
    return this.controls.valid;
  }
}
