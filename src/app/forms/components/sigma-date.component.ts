import { Component } from '@angular/core';
import { InputComponent } from './input.component';

@Component({
  selector: 'app-sigma-date',
  template: `
    <div [formGroup]="form" class="adcourse-form__input">
      <mat-form-field>
        <input
            [placeholder]="formInput.label"
            [formControlName]="formInput.key"
            matInput
            type="text">

          <mat-error
              *ngIf="controls.errors?.required">
              {{ formInput.label }} is <strong>required</strong>
          </mat-error>

        <mat-error
            *ngIf="controls.errors?.pattern && formInput.patternMessage && !controls.errors?.required">
          {{ formInput.label + ' ' + formInput.patternMessage }}
        </mat-error>

        <mat-error *ngIf="controls.errors?.pattern && !formInput.patternMessage && !controls.errors?.required">
          {{ formInput.label + ' format is not correct' }}
        </mat-error>

        <mat-error *ngIf="controls.errors?.fromDate && !controls.errors?.required && !controls.errors?.pattern">
          {{formInput.label}}'s value must be after {{controls.errors.fromDate?.fromDate | date}}
        </mat-error>

        <mat-error *ngIf="controls.errors?.toDate && !controls.errors?.required && !controls.errors?.pattern">
          {{formInput.label}}'s value must be before {{controls.errors.toDate?.toDate | date}}
        </mat-error>
      </mat-form-field>
    </div>
  `,
  styleUrls: ['./input.component.sass'],
})
export class SigmaDateComponent extends InputComponent {

  constructor() { super(); }
}


