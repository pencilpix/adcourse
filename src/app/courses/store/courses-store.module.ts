import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { coursesReducer } from './reducers';
import { CoursesEffects } from './effects';


@NgModule({
  imports: [
    CommonModule,
    EffectsModule.forFeature([CoursesEffects]),
    StoreModule.forFeature('courses', coursesReducer),
  ],
  declarations: []
})
export class CoursesStoreModule { }
