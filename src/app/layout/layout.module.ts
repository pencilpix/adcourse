import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule, Title } from '@angular/platform-browser';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material';
import {LayoutStoreModule } from './store/layout-store.module';


import { LayoutComponent } from './containers/layout.component';
import { NavComponent } from './components/nav/nav.component';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    RouterModule,
    MatToolbarModule,
    BrowserModule,
    LayoutStoreModule,
    MatButtonModule,
  ],
  providers: [
    Title,
  ],
  exports: [
   BrowserAnimationsModule,
   LayoutComponent,
  ],
  declarations: [LayoutComponent, NavComponent, FooterComponent]
})
export class LayoutModule { }
