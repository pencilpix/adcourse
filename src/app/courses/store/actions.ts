import { Action } from '@ngrx/store';
import { CoursesResponse } from '../models/course';

export const LOADING = '[courses] loading';
export const LOAD_COURSES = '[courses] load courses';
export const LOAD_COURSES_SUCCESS = '[courses] load courses success';
export const LOAD_COURSES_ERROR = '[courses] load courses error';
export const UPDATE_INDICATOR = '[courses] update indicator';
export const RESET_INDICATOR = '[courses] reset indicator';



export class Loading implements Action {
  readonly type = LOADING;

  constructor(public payload: boolean) {}
}


export class LoadCourses implements Action {
  readonly type = LOAD_COURSES;

  constructor(public payload: number) {}
}


export class LoadCoursesSuccess implements Action {
  readonly type = LOAD_COURSES_SUCCESS;

  constructor(public payload: CoursesResponse) {}
}


export class LoadCoursesError implements Action {
  readonly type = LOAD_COURSES_ERROR;

  constructor(public payload: string) {}
}


export class UpdateIndicator implements Action {
  readonly type = UPDATE_INDICATOR;
  constructor() {}
}

export class ResetIndicator implements Action {
  readonly type = RESET_INDICATOR;
  constructor() {}
}

export type Actions = Loading |
                  LoadCourses |
                  LoadCoursesSuccess |
                  LoadCoursesError |
                  UpdateIndicator |
                  ResetIndicator;
