import { InputTypeBase, InputTypeOptions } from './input-type';

export class InputSigmaNumber extends InputTypeBase<string> {
  controlType: 'sigmaNumber';
  type: string;
  min: number;
  max: number;

  constructor(options: InputTypeOptions<string> = {}) {
    super(options);
    this.type = 'number';
    this.min = options.min || 1;
    this.max = options.max || null;
  }
}
