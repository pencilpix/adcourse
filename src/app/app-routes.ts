import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';

export const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {
      title: 'Welcome to Adcourse | publish courses',
    },
  },

  {
    path: 'courses',
    loadChildren: './courses/courses.module#CoursesModule',
    // data: {
    //   title: 'Courses Ads | ADCOURSE',
    // },
  },

  {
    path: '**',
    component: NotFoundComponent,
    data: {
      title: 'Page is not found',
    },
  },
];
