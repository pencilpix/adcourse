export interface InputTypeOptions <T> {
  value?: T;
  key?: string;
  label?: string;
  required?: boolean;
  order?: number;
  controlType?: string;
  type?: string;
  min?: number;
  max?: number;
  email?: boolean;
  minLength?: number;
  maxLength?: number;
  pattern?: RegExp | string;
  fromDate?: Date | string;
  toDate?: Date | string;
  patternMessage?: string;
}

export class InputTypeBase <T> {
  value: T;
  label: string;
  key: string;
  required: boolean;
  order: number;
  controlType: string;
  type: string;
  min: number;
  max: number;
  pattern: RegExp|string;
  patternMessage: string;
  minLength: number;
  maxLength: number;
  fromDate: Date | string;
  toDate: Date | string;

  constructor(options: InputTypeOptions<T> = {}) {
    this.value = options.value;
    this.key = options.key || '';
    this.label = options.label || '';
    this.required = options.required;
    this.order = typeof options.order === 'undefined' ? 1 : options.order;
    this.controlType = options.controlType || '';
    this.type = options.type || '';
    this.min = options.min || null;
    this.max = options.max || null;
    this.minLength = options.minLength || null;
    this.maxLength = options.maxLength || null;
    this.fromDate = options.fromDate || '';
    this.toDate = options.toDate || '';
    this.patternMessage = options.patternMessage || '';
  }
}
