import { InputTypeOptions } from '../forms/models';


/**
 * these options that will control the create course
 * form and it should be fetched from api (if it's a large scale app)
 */
export const instructorOptions: InputTypeOptions<string>[] = [
  {
    controlType: 'sigmaText',
    type: 'text',
    order: 1,
    key: 'firstName',
    label: 'First Name',
    required: true,
  },

  {
    controlType: 'sigmaText',
    type: 'text',
    order: 2,
    key: 'lastName',
    label: 'Last Name',
    required: true,
  },


  {
    controlType: 'sigmaEmail',
    order: 3,
    key: 'email',
    label: 'Email Address',
    required: true,
  },


  {
    controlType: 'sigmaUrl',
    order: 4,
    key: 'avatar',
    label: 'Instructor Avatar',
    required: true,
  },
];

export const courseOptions: InputTypeOptions<string>[] = [
  {
    controlType: 'sigmaText',
    type: 'text',
    order: 1,
    key: 'title',
    label: 'Title',
    required: true,
  },

  {
    controlType: 'sigmaText',
    type: 'textarea',
    order: 2,
    key: 'description',
    label: 'Description',
    required: true,
  },

  {
    controlType: 'sigmaUrl',
    order: 3,
    key: 'image',
    label: 'Image',
    required: true,
  },

  {
    controlType: 'sigmaDate',
    order: 4,
    key: 'startAt',
    label: 'Started At',
    required: true,
    toDate: new Date(),
  },

  {
    controlType: 'sigmaDate',
    order: 5,
    key: 'endAt',
    label: 'end At',
    required: true,
    toDate: new Date(),
  },

  {
    controlType: 'sigmaNumber',
    order: 6,
    key: 'minSeats',
    label: 'Min Seats No.',
    required: true,
    min: 1,
  },


  {
    controlType: 'sigmaNumber',
    order: 7,
    key: 'maxSeats',
    label: 'Max Seats No.',
    required: true,
    min: 1,
  },
];
