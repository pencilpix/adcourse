import { FormsState, FormSteps } from './state';

const getValues = (steps, values = {}, checkForDefault: boolean = false) => {
  Object.values(steps).forEach((step: any[], i) => {
    values[i] = values[i] || {};
    step.forEach(item => {

      if (typeof item.value !== 'undefined' && checkForDefault) {
        item.defaultValue = item.value;
      }

      values[i][item.key] = item.value || '';
    });
  });

  return values;
};



const resetValues = (steps) => {
  const list = Object.values(steps);
  const newSteps = list.map((step: any) => {
    return step.map((item: any) => {
      return { ...item, value: item.defaultValue || '' };
    });
  }).reduce((cur, next, i) => ({...cur, [i]: next}), {});

  return {
    steps: newSteps,
    values: getValues(newSteps, {}),
  };
};



export function createReducersHelper<T>() {
  return {
    addSteps(steps: FormSteps<T>, state: FormsState<T>) {
      const values = getValues(steps, {}, true);
      return Object.assign(
        {},
        state,
        {
          steps,
          selectedStep: 0,
          errors: {},
          values,
          loading: false,
          stepsNo: Object.keys(steps).length,
        },
      );
    },

    addStep(step: any, state: FormsState<T>) {
      const length = Object.keys(state.steps).length;
      const steps = { ...state.steps, [length]: step };
      const values = getValues(steps, state.values, true);
      return Object.assign({}, state, { steps, values });
    },


    setLoading(loading: boolean, state: FormsState<T>) {
      return Object.assign({}, state, { loading });
    },


    setError(error, state) {
      const errors = { ...state.errors, ...error };
      return { ...state, errors };
    },

    gotoStep(no, state) {
      const length = Object.keys(state.steps).length;
      const selected = state.selectedStep;
      let newSelected = selected + no;

      if (newSelected > length - 1) {
        newSelected = length - 1;
      } else if (newSelected < 0) {
        newSelected = 0;
      }

      return { ...state, selectedStep: newSelected };
    },

    resetFormValues(state: FormsState<T>) {
      const { steps, values } = resetValues(state.steps);
      return {...state, steps, values, selectedStep: 0 };
    },


    updateValues(values, state: FormsState<T>) {
      const step: any = state.steps[state.selectedStep];
      const newStep = step.map(item => ({...item, value: values[item.key]}));
      const newSteps = { ...state.steps, [state.selectedStep]: newStep };
      const newValues = getValues(newSteps);
      return { ...state, values: newValues, steps: newSteps };
    }
  };
}
