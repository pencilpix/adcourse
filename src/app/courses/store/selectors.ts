import { createSelector, createFeatureSelector } from '@ngrx/store';
import { Course } from '../models/course';
import { State, adapter } from './state';


export const coursesState = createFeatureSelector<State>('courses');


export const {
  selectIds: courseIds,
  selectEntities: courseEntities,
  selectAll: allCourses,
  selectTotal: coursesCount,
} = adapter.getSelectors(coursesState);


export const coursesLoading = createSelector(
  coursesState,
  (state: State) => state.loading,
);


export const end = createSelector(
  coursesState,
  (state: State) => state.page > 0 && state.page >= state.last
);


export const indicator = createSelector(
  coursesState,
  (state: State) => state.indicator
);


export const page = createSelector(
  coursesState,
  (state: State) => state.page
);


export const loading = createSelector(
  coursesState,
  (state: State) => state.loading
);


export const hasMore = createSelector(
  indicator,
  coursesCount,
  (ind: number, count: number) => ind < count
);


export const courses = createSelector(
  allCourses,
  indicator,
  (list: Course[], ind: number) => list.slice(0, ind)
);

export const coursesError = createSelector(
  coursesState,
  (state: State) => state.error,
);
