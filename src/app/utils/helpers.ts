import * as CoursesModels from '../courses/models/course';


export function createCourse(data: CoursesModels.CourseData): CoursesModels.Course {
  return {
    id: data.id,
    firstName: data.first_name,
    lastName: data.last_name,
    email: data.email,
    title: data.title,
    description: data.description,
    avatar: data.avatar,
    image: data.image,
    startAt: data.start_at,
    endAt: data.end_at,
    minSeats: data.min_seats,
    maxSeats: data.max_seats,
  };
}


export function createCourseData(data: CoursesModels.Course): CoursesModels.CourseData {
  return {
    id: data.id,
    first_name: data.firstName,
    last_name: data.lastName,
    email: data.email,
    title: data.title,
    description: data.description,
    avatar: data.avatar,
    image: data.image,
    start_at: data.startAt,
    end_at: data.endAt,
    min_seats: data.minSeats,
    max_seats: data.maxSeats,
  };
}