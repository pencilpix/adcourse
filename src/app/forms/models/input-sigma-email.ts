import { InputTypeBase, InputTypeOptions } from './input-type';

export class InputSigmaEmail extends InputTypeBase<string> {
  controlType: 'sigmaEmail';
  type: string;
  pattern: RegExp | string;
  email: boolean;
  patternMessage: string;

  constructor(options: InputTypeOptions<string> = {}) {
    super(options);
    this.type = options['type'] || 'email';
    this.pattern = options.pattern || /@SigmaProIt\.com/;
    this.patternMessage = options.patternMessage || 'must be owned by SigmaProIt.com';
    this.email = true;
  }
}
