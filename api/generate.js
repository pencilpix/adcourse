const _ = require('underscore')
const faker = require('faker')
const fs = require('fs')
const { resolve } = require('path')
const moment = require('moment')

const imgIds = [693046, 696016, 693051, 695011, 690195, 692025, 695427, 693496, 697777]
const generateImg = () => {
  const no = _.random(8)
  const id = imgIds[no]
  return `https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-${id}.jpg`;

}


const formatDate = (date) => moment(date).format('YYYY-MM-DD')


const saveDb = (data) => {
  const dbData = JSON.stringify(data)
  const dbPath = resolve(__dirname, './db.json')
  return new Promise((resolve, reject) => {
    fs.writeFile(dbPath, dbData, (err) => {
      if (err) reject(err);
      console.log('[db generator]: json-server db is generated successfully')
      resolve(true)
    })
  })
}

const generate = () => {
  const data = {}
  const refDate = new Date('2018-10-1')

  data.courses = _.times(30, (n) => {
    const startDateRandom = _.random(20)
    const minSeatsNo = _.random(2, 10)
    const endDate = new Date('2019-10-01')
    const startDate = faker.date.between(refDate, endDate)

    return {
      id: ~~(n + (Math.random() * 1000) + Date.now()),
      first_name: faker.name.firstName(),
      last_name: faker.name.lastName(),
      email: faker.name.firstName().toLowerCase() + '@SigmaProIt.com',
      avatar: faker.image.avatar(),
      title: faker.lorem.words(_.random(2, 4)),
      description: faker.lorem.lines(_.random(2, 4)),
      image: generateImg(),
      start_at: formatDate(startDate),
      end_at: formatDate(faker.date.between(startDate, endDate)),
      min_seats: minSeatsNo,
      max_seats: minSeatsNo + _.random(3, 10),
    }
  })


  saveDb(data)
    .then(() => console.log('[db generator] generating db is done'))
    .catch(err => console.log('[db generator] failed\n', err))
}


generate();
