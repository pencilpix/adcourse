import { createFormsSelectors } from '../../forms/store';

const {
  form,
  steps,
  errors,
  values,
  value,
  error,
  loading,
  activeStep,
  activeStepValue,
  stepIndex,
  stepsNo,
} = createFormsSelectors<any>('createCourse');

export {
  form,
  steps,
  errors,
  values,
  value,
  error,
  activeStep,
  activeStepValue,
  loading,
  stepIndex,
  stepsNo
};


