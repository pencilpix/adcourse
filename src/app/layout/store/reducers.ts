import { State, initialState } from './state';
import {
  Actions,
  SET_TITLE,
  SET_URL_SUCCESS
} from './actions';

export function layoutReducer(state: State = initialState, action: Actions) {
  switch (action.type) {
    case SET_TITLE:
      return { ...state, title: action.payload };

    case SET_URL_SUCCESS:
      return { ...state, activeUrl: action.payload };

    default:
      return state;
  }
}
