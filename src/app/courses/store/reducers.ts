import { State, initialState, adapter } from './state';
import {
  Actions,
  LOADING,
  LOAD_COURSES_SUCCESS,
  LOAD_COURSES_ERROR,
  UPDATE_INDICATOR,
  RESET_INDICATOR
} from './actions';


export function coursesReducer(state: State = initialState, action: Actions) {
  let newState;

  switch (action.type) {
    case LOADING:
      return { ...state, loading: action.payload };

    case LOAD_COURSES_SUCCESS:
      newState = {
        ...state,
        loading: false,
        error: '',
        first: action.payload.first,
        last: action.payload.last,
        page: action.payload.page,
        indicator: state.indicator,
      };
      return adapter.addMany(action.payload.list, newState);


    case LOAD_COURSES_ERROR:
      return { ...state, loading: false, error: action.payload };


    case UPDATE_INDICATOR:
      return { ...state, indicator: state.indicator + 10 };


    case RESET_INDICATOR:
      return { ...state, indicator: 0 };


    default:
      return state;
  }
}


