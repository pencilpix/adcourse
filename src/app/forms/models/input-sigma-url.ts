import { InputTypeBase, InputTypeOptions } from './input-type';


export class InputSigmaUrl extends InputTypeBase<string> {
  controlType: 'sigmaUrl';
  type: string;
  pattern: RegExp | string;
  patternMessage: string;

  constructor(options: InputTypeOptions<string> = {}) {
    super(options);
    this.type = 'text';
    this.pattern = options.pattern || /(https?:\/\/.*\.(?:png|jpg))/i;
    this.patternMessage = options.patternMessage || 'url must be valid png or jpg image file url';
  }
}
