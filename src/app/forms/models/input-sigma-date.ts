import { InputTypeBase, InputTypeOptions } from './input-type';

export class InputSigmaDate extends InputTypeBase<string> {
  controlType: 'sigmaDate';
  type: string;
  pattern: RegExp | string;
  patternMessage: string;
  fromDate: Date | string;
  toDate: Date | string;

  constructor(options: InputTypeOptions<string> = {}) {
    super(options);
    this.type = 'text';
    this.pattern = /^([0-9]{4})-([0]?[1-9]|[1][0-2])-([0]?[1-9]|[1|2][0-9]|[3][0|1])$/;
    this.fromDate = options.fromDate || '1990-01-01';
    this.toDate = options.toDate || '';
    this.patternMessage = options.patternMessage || ' must be in valid date format YYYY-MM-DD';
  }
}
