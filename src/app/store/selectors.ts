import { createSelector, createFeatureSelector } from '@ngrx/store';
import { RouterReducerState } from '@ngrx/router-store';


export const getRouterState = createFeatureSelector<RouterReducerState>('router');
export const getCurrentUrl = createSelector(
  getRouterState,
  (state: RouterReducerState) => state.state && state.state.url,
);
