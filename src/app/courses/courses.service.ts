import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { CoursesDataResponse } from './models/course';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import parse from 'parse-link-header';



@Injectable()
export class CoursesService {
  listUrl = '/api/courses';

  constructor(private http: HttpClient) { }

  fetchCourses(pageNo): Observable<CoursesDataResponse> {
    const page = pageNo || 1;
    const params = { _page: `${page}` };
    return this.http.get(this.listUrl, { params, observe: 'response'})
      .pipe(map((res: HttpResponse<any>) => {
          const link = parse(res.headers.get('Link'));
          const first = parseInt(link.first._page, 10);
          const last = parseInt(link.last._page, 10);
          link.total = parseInt(res.headers.get('x-total-count'), 10);
          return ({ total: link.total, first, last, page, list: res.body });
        }));
  }
}
