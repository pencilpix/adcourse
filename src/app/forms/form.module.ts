import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsComponent } from './containers/forms.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCardModule,
  MatProgressSpinnerModule,
  MatIconModule,
  MatFormFieldModule,
  MatInputModule,
} from '@angular/material';
import { FormsInputControlService } from './forms-input-control.service';
import { FormsInputMakerService } from './forms-input-maker.service';
import { InputComponent } from './components/input.component';
import { SigmaEmailComponent } from './components/sigma-email.component';
import { SigmaNumberComponent } from './components/sigma-number.component';
import { SigmaUrlComponent } from './components/sigma-url.component';
import { SigmaDateComponent } from './components/sigma-date.component';
import { SigmaTextComponent } from './components/sigma-text.component';
import { SyncFormDirective } from './directives/sync-form.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
  ],

  providers: [
    FormsInputControlService,
    FormsInputMakerService
  ],

  exports: [
    FormsComponent,
  ],

  declarations: [
    SyncFormDirective,
    FormsComponent,
    InputComponent,
    SigmaEmailComponent,
    SigmaNumberComponent,
    SigmaUrlComponent,
    SigmaDateComponent,
    SigmaTextComponent,
  ]
})
export class FormModule { }
